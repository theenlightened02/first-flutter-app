import 'package:flutter/material.dart';
import './custom_text_widget.dart';
import './immutable_widget.dart';

class BasicScreen extends StatelessWidget {
  const BasicScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        title: const Text('Welcome To Flutter'),
        actions: const <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.edit),
          )
        ],
      ),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image.asset('assets/images/beach.jpg'),
            const CustomTextWidget(),
            // AspectRatio(aspectRatio: 1, child: ImmutableWidget()),
            /*LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                return AspectRatio(
                  aspectRatio: 1,
                  child: ImmutableWidget(),  // Ensure this widget is also responsive to the given constraints.
                );
              },
            ),*/
            Flexible(
              child: Center(
                child: AspectRatio(
                  aspectRatio: 1,
                  child: ImmutableWidget(),
                ),
              ),
            )
          ]
          // children: [
          //   AspectRatio(
          //     aspectRatio: 1,
          //     child: ImmutableWidget(),
          //   ),
          //   const TextLayout()
          // ],
          ),
      drawer: Drawer(
        child: Container(
          color: Colors.lightBlue,
          child: const Center(
            child: Text("I'm a Drawer"),
          ),
        ),
      ),
    );
  }
}
