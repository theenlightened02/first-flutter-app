import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// [CustomTextWidget] displays various styles of text to demonstrate
/// various text styling options in Flutter.
class CustomTextWidget extends StatelessWidget {
  const CustomTextWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // Display a greeting message with a custom font.
        Text('Hello, World!', style: GoogleFonts.leckerliOne(fontSize: 40)),
        // Display a smaller text with a different custom font.
        Text('Text can wrap without issue', style: GoogleFonts.aBeeZee(fontSize: 20)),
        // Display a longer, unstyled text.
        const Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        const Divider(),
        // Display a RichText with mixed styling.
        RichText(
            text: const TextSpan(
              text: 'Flutter text is ',
              style: TextStyle(fontSize: 24, color: Colors.black),
              children: <TextSpan>[
                TextSpan(
                  text: 'really ',
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                  children: [
                    TextSpan(
                      text: 'powerful.',
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          decorationStyle: TextDecorationStyle.double,
                          fontSize: 40),
                    ),
                  ],
                ),
              ],
            )
        ),
      ],
    );
  }
}
